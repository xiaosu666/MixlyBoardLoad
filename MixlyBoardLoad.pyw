# Mixly板卡一键导入工具
# 小苏制作
#
import os,shutil
import tkinter as tk
from tkinter import filedialog

app_path = os.path.abspath(".").replace("\\","/")
board_path = app_path + "/board/"
flag = 0    # Mixly软件路径正确标志

# Mixly软件boards.json路径
boards_json_path = ""
# XSrobot板卡配置信息
XSrobot_json = \
"""
    {
        "boardImg": "./board/XSrobot_std/media/XSrobot_std.png",
        "boardType": "XSrobot std",
        "boardIndex": "./board/XSrobot_std/index_board_Arduino_ESP32.html",
        "env": {
            "electron": true,
            "web": false,
            "webCompiler": true,
            "webSocket": true
        }
    },"""


# 打开并检查Mixly软件路径
def open_mixly_path():
    global flag
    global boards_json_path
    text_info.insert(tk.END,"\n")
    mixly_path.set(filedialog.askdirectory())
    for files in os.listdir(mixly_path.get()):
        if "Mixly.exe" in files:
            text_info.insert(tk.END,"Mixly路径正确！\n已找到" + mixly_path.get() + "/Mixly.exe\n")
            boards_json_path = mixly_path.get() + "/resources/app/src/boards.json"
            flag = 1
            break
        else:
            flag = 0
    if flag == 0:
        text_info.insert(tk.END,"Mixly路径错误！未找到Mixly.exe,请重新选择！\n")
        flag = 0
    text_info.see(tk.END)

# 加载板卡到Mixly中
def load_board():
    global flag
    global XSrobot_json
    global board_path
    global boards_json_path
    text_info.insert(tk.END,"\n")
    if flag == 1:
        text_info.insert(tk.END,"正在导入...\n")
        text_info.insert(tk.END,"读取"+boards_json_path+"\n")
        # 写入XSrobot配置信息
        with open(boards_json_path, 'r' ) as f:
            boards_json = f.read()
        if XSrobot_json in boards_json: # 检查是否已包含XSrobot配置信息
            text_info.insert(tk.END,"已包含XSrobot配置信息\n")
        else:
            # 写入XSrobot配置信息到boards.json
            text_info.insert(tk.END,"写入"+boards_json_path+"\n")
            temp_list = list(boards_json)
            temp_list.insert(1,XSrobot_json)    # 插入XSrobot配置信息到boards.json第一项中
            temp_json = "".join(temp_list)
            with open(boards_json_path, 'w' ) as f:
                f.write(temp_json)
                text_info.insert(tk.END,"写入XSrobot配置信息成功\n")
        # 复制板卡文件到Mixly软件中
        text_info.insert(tk.END,"正在复制文件...\n")
        mixly_board_path = mixly_path.get() + "/resources/app/src/board/"
        text_info.insert(tk.END,"目标"+mixly_board_path+"\n")
        text_info.insert(tk.END,"复制板卡文件中，请勿关闭...\n")
        #print("xcopy " + board_path.replace("/","\\") + " " + mixly_board_path.replace("/","\\") )
        # 复制文件返回结果
        with os.popen("xcopy " + board_path.replace("/","\\") + " " + mixly_board_path.replace("/","\\") + " /s") as f:
            result = f.read()
            if "复制了 0 个文件" in result:
                text_info.insert(tk.END,"Mixly中已含有XSrobot板卡文件\n")
                text_info.insert(tk.END,result)
            else:
                text_info.insert(tk.END,result)
            text_info.see(tk.END)
        text_info.insert(tk.END,"复制板卡文件成功\n")
        text_info.insert(tk.END,"导入成功！\n")
        
    else:
        text_info.insert(tk.END,"请先选择Mixly软件路径！\n")

    text_info.see(tk.END)

def delete_board():
    global flag
    global XSrobot_json
    global boards_json_path
    text_info.insert(tk.END,"\n")
    if flag == 1:
        text_info.insert(tk.END,"正在删除...\n")
        text_info.insert(tk.END,"读取"+boards_json_path+"\n")
        with open(boards_json_path, 'r' ) as f:
            boards_json = f.read()
        if XSrobot_json in boards_json: # 检查boards.json里是否已有XSrobot配置信息
            text_info.insert(tk.END,"已找到XSrobot配置信息\n")
            text_info.insert(tk.END,"删除配置信息"+boards_json_path+"\n")
            temp_json = boards_json.replace(XSrobot_json,"")
            with open(boards_json_path, 'w' ) as f:
                f.write(temp_json)
                text_info.insert(tk.END,"删除配置信息成功\n")
        else:
            text_info.insert(tk.END,"未找到XSrobot配置信息\n")
        text_info.insert(tk.END,"正在删除板卡文件...\n")
        mixly_XSrobot_path = mixly_path.get() + "/resources/app/src/board/XSrobot_std"
        with os.popen("rmdir /s/q " + mixly_XSrobot_path.replace("/","\\")) as f:
            result = f.read()
            if result == "":
                text_info.insert(tk.END,"删除成功！")
            else:
                text_info.insert(tk.END,"删除文件失败！")
    else:
        text_info.insert(tk.END,"请先选择Mixly软件路径！\n")

    text_info.see(tk.END)

main_window = tk.Tk()
main_window.title("小苏机器人Mixly板卡导入工具")
main_window.iconbitmap(app_path + "./load.ico")
main_window.geometry("400x400")
main_window.resizable(False,False)

frame0 = tk.Frame(main_window)
frame0.pack(side="top", fill="both",expand=True)

frame1 = tk.Frame(frame0)
frame1.pack(side="top",fill="x")

label_path = tk.Label(frame1,text="Mixly路径：")
label_path.pack(side="left")

mixly_path = tk.StringVar(main_window, value="D:/mixly")
entry_path = tk.Entry(frame1, textvariable=mixly_path)
entry_path.pack(side="left", fill="x", expand=True)

btn_path = tk.Button(frame0,text="选择Mixly根目录", activebackground="white",command=open_mixly_path)
btn_path.pack(side="top", fill="x")

btn_load = tk.Button(frame0,text="导入XSrobot板卡", activebackground="white", command=load_board)
btn_load.pack(side="top", fill="x")

btn_delete = tk.Button(frame0,text="删除XSrobot板卡", activebackground="white", command=delete_board)
btn_delete.pack(side="top", fill="x")

label_copyright = tk.Label(frame0,text="Copyright © 2023 小苏. All rights reserved.")
label_copyright.pack(side="bottom")

info_text = tk.StringVar(main_window)
text_info = tk.Text(frame0,bg="black",fg="white")
#text_info.config(state="disabled")
text_info.pack(side="top",fill="both")
text_info.insert(tk.END,"Mixly板卡导入工具V1.0  XSrobot板卡版本V1.0\n")

main_window.mainloop()